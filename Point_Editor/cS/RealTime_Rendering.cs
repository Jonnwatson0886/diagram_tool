﻿using ImageGridUserControl.Classes.Images;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace corner
{
    class RealTime_Render : INotifyPropertyChanged
    {
        #region var
        private string path;
        public List<Ellipse> ellipses;
        private Ellipse ellipse;
        private DispatcherTimer timer;
        private Canvas Holder;
        public List<HardwareImage> Images;
        System.Windows.Controls.Image image ;
        public Brush fill
        {
            get
            {
                return _brush;
            }
            set
            {
                if (_brush != value)
                {
                    _brush = value;
                    onPropertyChanged();
                }
            }
        }
        private Brush _brush;
        public bool stopper;
        public RealTime_Render DataContext { get; }
        private double _X;
        public double X
        {
            get { return _X; }
            set
            {
                if (_X != value)
                {
                    _X = value;
                    onPropertyChanged();
                }
            }
        }
        private double _Y;
        public double Y
        {
            get { return _Y; }
            set
            {
                if (_Y != value)
                {
                    _Y = value;
                    onPropertyChanged();
                }
            }
        }
        private int _Line_Size;
        public int Line_Size
        {
            get { return _Line_Size; }
            set
            {
                if (value != _Line_Size)
                {
                    _Line_Size = value;
                    onPropertyChanged();
                }
            }

        }
        public double imageWidth
        {
            get
            {
                return _imageWidth;
            }
            set
            {
                if (value != _imageWidth)
                {
                    _imageWidth = value;
                    onPropertyChanged();
                }
            }
        }
        private double _imageWidth;
        public double imageHeight
        {
            get
            {
                return _imageHeight;
            }
            set
            {
                if (value != _imageHeight)
                {
                    _imageHeight = value;
                    onPropertyChanged();
                }
            }
        }
        private double _imageHeight;
        #endregion
        #region property changed event 
        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged([CallerMemberName] string property_name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property_name));
        }
        #endregion
        #region constuctor
        public RealTime_Render(ref Canvas _Holder)
        {
            path = ("C:\\Users\\jonny\\Desktop\\git\\diagram_tool\\testing_grid\\testing_grid\\bin\\Debug\\..\\..\\Diagram_Textures\\DefaultImageProperties.xml");
            Line_Size = 5;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1);
            Holder = _Holder;
            X = 0;
            Y = 0;
            stopper = false;
            fill = new SolidColorBrush(Colors.Yellow);
            ellipses = new List<Ellipse>();
            imageWidth = 400;
            imageHeight = 400;
            Images = Get_Images();
        }
        #endregion

        #region rendering Engine
        public void start_timer(object sender)
        {
            timer.Start();
            if (sender is Canvas)
            {
                fill = new SolidColorBrush(Colors.Green);
                timer.Tick += tick;
            }
        }
        public void tick(object sender, EventArgs e)
        {
            if (stopper)
            {
                Holder.Children.Clear();
                if (image != null)
                {
                    Holder.Children.Add(image);
                }
                ellipse = CreateE();
                if (ellipse != null)
                {
                    Point point = Mouse.GetPosition(Holder);
                    relocate();
                }
                draw();
            }
            else
            {
                end();
            }
        }
        /// <summary>
        /// creates an elipse 
        /// </summary>
        /// <param name="brush"></param>
        /// <returns></returns>
        public void end()
        {
            timer.Stop();
            fill = new SolidColorBrush(Colors.Red);
        }
        #endregion
        private Ellipse CreateE()
        {
            if (ellipse != null)
            {
                ellipse.Height = 10;
                ellipse.Width = 10;
                // Create a blue and a black Brush    
                SolidColorBrush blackBrush = new SolidColorBrush();
                blackBrush.Color = Colors.Black;
                // Set Ellipse's width and color    
                ellipse.StrokeThickness = 2;
                ellipse.Stroke = blackBrush;
                return ellipse;
            }
            else
            {
                return null;
            }
        }
        private void relocate()
        {
            Point point = Mouse.GetPosition(Holder);

            X = point.X - (ellipse.Width / 2);
            Y = point.Y - (ellipse.Height / 2);
            X = Math.Floor(X);
            Y = Math.Floor(Y);
            X = boardControl(X, Math.Round(Holder.ActualWidth) - 10);
            Y = boardControl(Y, Math.Round(Holder.ActualHeight) - 10);
            Canvas.SetLeft(ellipse, X);
            Canvas.SetTop(ellipse, Y);


        }
        private double boardControl(double input, double topLimit)
        {
            if (input < 0)
            {
                return 0;
            }
            else if (input > topLimit)
            {
                return topLimit;
            }
            return input;
        }
        /// <summary>
        /// sets the line being edited to the line object passed though.
        /// </summary>
        /// <param name="line"></param>
        public void set_Index(int i)
        {
            ellipse = ellipses[i];
        }
        private void draw()
        {
            foreach (Ellipse i in ellipses)
            {
                Holder.Children.Add(i);
            }
        }
        //loads in the xml file
        public List<HardwareImage> Get_Images()
        {
            
            List<HardwareImage> images = new List<HardwareImage>();
            ImageList list = ImageList.LoadList(path);
            foreach (HardwareImage itr in list.Images)
            {
                images.Add(itr);
            }
            return images;
        }

        public void set_Image(string selection)
        {
            Holder.Children.Clear();
            //get the image
            HardwareImage H = Images.Find(cl => cl.ImageType == selection);
            HardwareImage Copy = H.Copy();

            //create the image in the background 
            image = Copy.GetImage(800 ,800);
            image.Width = Copy.width*Copy.size;
            image.Height = Copy.height*Copy.size;
            Canvas.SetLeft(image, 0);
            Canvas.SetTop(image, 0);

            Holder.Children.Add(image);
            ellipses.Clear();
        }
        public void newElispe()
        {
            Brush mapable;
            Random r = new Random();
            mapable = new SolidColorBrush(Color.FromRgb((byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256)));
            Ellipse elispe = new Ellipse();
            elispe.Fill = mapable;
            ellipses.Add(elispe);
        }

        //public bool SavePoints()
        //{
        //    ImageList.SaveDefaultList()
        //    {

        //    }
        //}
    }
}

