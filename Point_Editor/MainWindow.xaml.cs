﻿using ImageGridUserControl.Classes.Images;
using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;


namespace corner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Vars
        RealTime_Render realTime_;
        #endregion
        #region constructor
        public MainWindow()
        {
            InitializeComponent();
            realTime_ = new RealTime_Render(ref testBed);
            DataContext = realTime_;
            StringBuilder file = new StringBuilder();
            file.Append(System.IO.Directory.GetCurrentDirectory());
            file.Append("\\..\\..\\Diagram_Textures\\DefaultImageProperties.xml");
            getHardware();

        }
        #endregion
        private void click(object sender, MouseButtonEventArgs e)
        {
            if (!realTime_.stopper)
            {
                realTime_.stopper = true;
                realTime_.start_timer(sender);
            }
            else
            {
                realTime_.stopper = false;
            }
        }
        private void Add_Line_Point_Click(object sender, RoutedEventArgs e)
        {
            realTime_.newElispe();
        }

        private void Elispe_Box_DropDownOpened(object sender, EventArgs e)
        {
            Elispe_Number_Selector.Items.Clear();
            if (realTime_.ellipses.Count != 0)
            {
                for (int itr = 0; itr < realTime_.ellipses.Count; itr++)
                {
                    Elispe_Number_Selector.Items.Add(itr + 1);
                }
            }
        }

        /// <summary>
        /// changes the elipse being edited to the one selected in the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Elispe_Box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Elispe_Number_Selector.SelectedItem != null)
            {
                realTime_.set_Index((int)Elispe_Number_Selector.SelectedItem - 1);
            }
        }
        /// <summary>
        /// sets the image rendered onto canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Select_image_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            realTime_.set_Image(select_image.SelectedItem.ToString());
        }

        /// <summary>
        /// runs function that makes the animation engine draw in images from xml file
        /// </summary>
        private void getHardware()
        {
            foreach (HardwareImage itr in realTime_.Images)
            {
                select_image.Items.Add(itr.ImageType);
            }
        }
    }
}