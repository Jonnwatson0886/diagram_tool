﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ImageGridUserControl.Classes
{

    public class XML_Saver
    {
        /// <summary>
        /// jons sameful first serialzer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ToSerialize"></param>
        /// <returns></returns>
        public static string SerializeObject<T>(T ToSerialize)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(ToSerialize.GetType());

                using (Utf8StringWriter utf8StringWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(utf8StringWriter, ToSerialize);
                    return utf8StringWriter.ToString();
                }
            }
            catch
            {
                throw;
            }
        }

        // It is important to override the XML string writer to UTF-8 for better compatiablity with xmlDocument
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

    }
}
