﻿using ImageGridUserControl;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace testing_grid
{
    public partial class Form1 : Form
    {
        string[] lines;

        public Form1()
        {
            InitializeComponent();
            elementHost1.Visible = true;

            dataGrid1.StartupDataGrid();
            dataGrid1.GridChangedEvent += DataGrid1_GridChangedEvent;
            dataGrid1.ReportMouseEvent += DataGrid1_ReportMouseEvent;

            txtBoxState.Text = "In Development Mode";
            lines = new string[2];
            
            List<string> names = new List<string>();
            names.AddRange(dataGrid1.GetInstrumentNames().ToArray());
            comboTypeToAdd.Items.Clear();
            foreach (string name in names) comboTypeToAdd.Items.Add(name);
        }

        private void DataGrid1_ReportMouseEvent(double X, double Y)
        {
            lines[0] = X.ToString("0.000")+" , "+ Y.ToString("0.000");
            rTBgridInfo.Lines = lines;
        }

        private void DataGrid1_GridChangedEvent(ImageGridUserControl.Classes.Events.GridInfo CurrentInfo)
        {
            lines[1] = CurrentInfo.SelectedInstrumentName;
            rTBgridInfo.Lines = lines;

            //Designer Mode Only
            comboStartConnection.Items.Clear();
            comboStartConnection.Items.AddRange(CurrentInfo.designerModeInfo.Connections);
            if(comboStartConnection.Items.Count>0) comboStartConnection.SelectedIndex = 0;

            comboEndConnection.Items.Clear();
            comboEndConnection.Items.AddRange(CurrentInfo.designerModeInfo.Connections);
            if (comboEndConnection.Items.Count > 0) comboEndConnection.SelectedIndex = 0;

            comboCurrentRoutes.Items.Clear();
            comboCurrentRoutes.Items.AddRange(CurrentInfo.designerModeInfo.CompletedConnections);
            if (comboCurrentRoutes.Items.Count > 0) comboCurrentRoutes.SelectedIndex = 0;


        }

        private void BtnChangeDiagramState_Click(object sender, EventArgs e)
        {
            try
            {
                
                GridState State = dataGrid1.DiagramUseMode;
                if (State == GridState.Customer)
                {
                    dataGrid1.change_Grid_State(GridState.Developer);
                    txtBoxState.Text = "In Development Mode";
                    groupDesignerMode.Enabled = true;
                }
                else
                {
                    dataGrid1.change_Grid_State(GridState.Customer);
                    txtBoxState.Text = "In Customer Mode Mode";
                    groupDesignerMode.Enabled = false;
                }

            }
            catch(Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void BtnLoadDiagram_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                fileDialog.Filter = "(*.XML) | *.XML";

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    dataGrid1.LoadDiagram(fileDialog.FileName);
                }
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void BtnSaveDiagram_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFile = new SaveFileDialog();

                saveFile.FileName = "diagram_Save";
                saveFile.DefaultExt = "XML";
                saveFile.Filter = "(.XML)| .XML";

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    dataGrid1.SaveDiagram(saveFile.FileName);
                }

            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void BtnCreateRoute_Click(object sender, EventArgs e)
        {
            try
            {
                string Start  = comboStartConnection.SelectedItem.ToString();
                string End = comboEndConnection.SelectedItem.ToString();

                if (dataGrid1.TestRoute(Start,  End))
                {
                    dataGrid1.CreateRoute(Start, End);
                }
        
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void BtnRemoveRoute_Click(object sender, EventArgs e)
        {
            try
            {
                dataGrid1.RemoveRoute(comboCurrentRoutes.SelectedItem.ToString());
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void BtnResetDiagram_Click(object sender, EventArgs e)
        {
            try
            {
                dataGrid1.ResetDiagram();
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
        }

        private void ComboSecondarySelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboStartConnection.SelectedItem != null)
            {
               dataGrid1.SecondarySelectionUpdated(comboStartConnection.SelectedItem.ToString());
            }
            else
            {
               dataGrid1.SecondarySelectionUpdated("");
            }
        }
        private void SplitContainer1_Panel1_Resize(object sender, EventArgs e)
        {
            try
            {
                int w = splitContainer1.Panel1.Width;
                int h = splitContainer1.Panel1.Height;

                dataGrid1.SetSize(w, h);
            }
            catch (Exception Ex) { HandleException(Ex); }
        }

        //private void ListViewInstruments_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (comboTypeToAdd.SelectedItems.Count > 0)
        //        {
        //            string InstrumentType = listViewInstruments.SelectedItems[0].Text;
        //            dataGrid1.InstrumentSelectionUpdated(InstrumentType);
        //        }
        //    }
        //    catch (Exception Ex) { HandleException(Ex); }
        //}

        private void ComboTypeToAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboTypeToAdd.SelectedItem.ToString()!="")
                {
                    string InstrumentType = comboTypeToAdd.SelectedItem.ToString();   //listViewInstruments.SelectedItems[0].Text;
                    dataGrid1.InstrumentSelectionUpdated(InstrumentType);
                }
            }
            catch (Exception Ex) { HandleException(Ex); }
        }

        private void HandleException(Exception Ex)
        {
            MessageBox.Show(Ex.Message + "; " + Ex.Source, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


    }
}
