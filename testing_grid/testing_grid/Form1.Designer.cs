﻿namespace testing_grid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChangeDiagramState = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.dataGrid1 = new ImageGridUserControl.DataGrid();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupGeneral = new System.Windows.Forms.GroupBox();
            this.rTBgridInfo = new System.Windows.Forms.RichTextBox();
            this.txtBoxState = new System.Windows.Forms.TextBox();
            this.btnLoadDiagram = new System.Windows.Forms.Button();
            this.groupDesignerMode = new System.Windows.Forms.GroupBox();
            this.ComboSecondarySelection = new System.Windows.Forms.ComboBox();
            this.listViewInstruments = new System.Windows.Forms.ListView();
            this.btnResetDiagram = new System.Windows.Forms.Button();
            this.btnSaveDiagram = new System.Windows.Forms.Button();
            this.btnRemoveRoute = new System.Windows.Forms.Button();
            this.btnCreateRoute = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupGeneral.SuspendLayout();
            this.groupDesignerMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnChangeDiagramState
            // 
            this.btnChangeDiagramState.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnChangeDiagramState.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChangeDiagramState.Location = new System.Drawing.Point(3, 63);
            this.btnChangeDiagramState.Name = "btnChangeDiagramState";
            this.btnChangeDiagramState.Size = new System.Drawing.Size(394, 27);
            this.btnChangeDiagramState.TabIndex = 1;
            this.btnChangeDiagramState.Text = "Change User Mode";
            this.btnChangeDiagramState.UseVisualStyleBackColor = true;
            this.btnChangeDiagramState.Click += new System.EventHandler(this.BtnChangeDiagramState_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.elementHost1);
            this.splitContainer1.Panel1.Resize += new System.EventHandler(this.SplitContainer1_Panel1_Resize);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1286, 689);
            this.splitContainer1.SplitterDistance = 882;
            this.splitContainer1.TabIndex = 3;
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(882, 689);
            this.elementHost1.TabIndex = 3;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.dataGrid1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupGeneral);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupDesignerMode);
            this.splitContainer2.Size = new System.Drawing.Size(400, 689);
            this.splitContainer2.SplitterDistance = 215;
            this.splitContainer2.TabIndex = 9;
            // 
            // groupGeneral
            // 
            this.groupGeneral.Controls.Add(this.rTBgridInfo);
            this.groupGeneral.Controls.Add(this.btnChangeDiagramState);
            this.groupGeneral.Controls.Add(this.txtBoxState);
            this.groupGeneral.Controls.Add(this.btnLoadDiagram);
            this.groupGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupGeneral.Location = new System.Drawing.Point(0, 0);
            this.groupGeneral.Name = "groupGeneral";
            this.groupGeneral.Size = new System.Drawing.Size(400, 215);
            this.groupGeneral.TabIndex = 8;
            this.groupGeneral.TabStop = false;
            // 
            // rTBgridInfo
            // 
            this.rTBgridInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.rTBgridInfo.Location = new System.Drawing.Point(3, 90);
            this.rTBgridInfo.Name = "rTBgridInfo";
            this.rTBgridInfo.Size = new System.Drawing.Size(394, 121);
            this.rTBgridInfo.TabIndex = 7;
            this.rTBgridInfo.Text = "";
            // 
            // txtBoxState
            // 
            this.txtBoxState.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtBoxState.Location = new System.Drawing.Point(3, 43);
            this.txtBoxState.Name = "txtBoxState";
            this.txtBoxState.Size = new System.Drawing.Size(394, 20);
            this.txtBoxState.TabIndex = 8;
            // 
            // btnLoadDiagram
            // 
            this.btnLoadDiagram.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLoadDiagram.Location = new System.Drawing.Point(3, 16);
            this.btnLoadDiagram.Name = "btnLoadDiagram";
            this.btnLoadDiagram.Size = new System.Drawing.Size(394, 27);
            this.btnLoadDiagram.TabIndex = 6;
            this.btnLoadDiagram.Text = "Load in Exsisting Diagram";
            this.btnLoadDiagram.UseVisualStyleBackColor = true;
            this.btnLoadDiagram.Click += new System.EventHandler(this.BtnLoadDiagram_Click);
            // 
            // groupDesignerMode
            // 
            this.groupDesignerMode.Controls.Add(this.button1);
            this.groupDesignerMode.Controls.Add(this.ComboSecondarySelection);
            this.groupDesignerMode.Controls.Add(this.listViewInstruments);
            this.groupDesignerMode.Controls.Add(this.btnResetDiagram);
            this.groupDesignerMode.Controls.Add(this.btnSaveDiagram);
            this.groupDesignerMode.Controls.Add(this.btnRemoveRoute);
            this.groupDesignerMode.Controls.Add(this.btnCreateRoute);
            this.groupDesignerMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupDesignerMode.Location = new System.Drawing.Point(0, 0);
            this.groupDesignerMode.Name = "groupDesignerMode";
            this.groupDesignerMode.Size = new System.Drawing.Size(400, 470);
            this.groupDesignerMode.TabIndex = 5;
            this.groupDesignerMode.TabStop = false;
            this.groupDesignerMode.Text = "Designer Mode Controls";
            // 
            // ComboSecondarySelection
            // 
            this.ComboSecondarySelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComboSecondarySelection.FormattingEnabled = true;
            this.ComboSecondarySelection.Location = new System.Drawing.Point(3, 366);
            this.ComboSecondarySelection.Name = "ComboSecondarySelection";
            this.ComboSecondarySelection.Size = new System.Drawing.Size(394, 21);
            this.ComboSecondarySelection.TabIndex = 2;
            this.ComboSecondarySelection.SelectedIndexChanged += new System.EventHandler(this.ComboSecondarySelection_SelectedIndexChanged);
            // 
            // listViewInstruments
            // 
            this.listViewInstruments.Dock = System.Windows.Forms.DockStyle.Top;
            this.listViewInstruments.FullRowSelect = true;
            this.listViewInstruments.GridLines = true;
            this.listViewInstruments.Location = new System.Drawing.Point(3, 124);
            this.listViewInstruments.Name = "listViewInstruments";
            this.listViewInstruments.Size = new System.Drawing.Size(394, 242);
            this.listViewInstruments.TabIndex = 10;
            this.listViewInstruments.UseCompatibleStateImageBehavior = false;
            this.listViewInstruments.View = System.Windows.Forms.View.List;
            this.listViewInstruments.SelectedIndexChanged += new System.EventHandler(this.ListViewInstruments_SelectedIndexChanged);
            // 
            // btnResetDiagram
            // 
            this.btnResetDiagram.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnResetDiagram.Location = new System.Drawing.Point(3, 97);
            this.btnResetDiagram.Name = "btnResetDiagram";
            this.btnResetDiagram.Size = new System.Drawing.Size(394, 27);
            this.btnResetDiagram.TabIndex = 6;
            this.btnResetDiagram.Text = "Start New Diagram";
            this.btnResetDiagram.UseVisualStyleBackColor = true;
            this.btnResetDiagram.Click += new System.EventHandler(this.BtnResetDiagram_Click);
            // 
            // btnSaveDiagram
            // 
            this.btnSaveDiagram.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSaveDiagram.Location = new System.Drawing.Point(3, 70);
            this.btnSaveDiagram.Name = "btnSaveDiagram";
            this.btnSaveDiagram.Size = new System.Drawing.Size(394, 27);
            this.btnSaveDiagram.TabIndex = 5;
            this.btnSaveDiagram.Text = "save current diagram";
            this.btnSaveDiagram.UseVisualStyleBackColor = true;
            this.btnSaveDiagram.Click += new System.EventHandler(this.BtnSaveDiagram_Click);
            // 
            // btnRemoveRoute
            // 
            this.btnRemoveRoute.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRemoveRoute.Location = new System.Drawing.Point(3, 43);
            this.btnRemoveRoute.Name = "btnRemoveRoute";
            this.btnRemoveRoute.Size = new System.Drawing.Size(394, 27);
            this.btnRemoveRoute.TabIndex = 4;
            this.btnRemoveRoute.Text = "Remove hardware Route";
            this.btnRemoveRoute.UseVisualStyleBackColor = true;
            this.btnRemoveRoute.Click += new System.EventHandler(this.BtnRemoveRoute_Click);
            // 
            // btnCreateRoute
            // 
            this.btnCreateRoute.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCreateRoute.Location = new System.Drawing.Point(3, 16);
            this.btnCreateRoute.Name = "btnCreateRoute";
            this.btnCreateRoute.Size = new System.Drawing.Size(394, 27);
            this.btnCreateRoute.TabIndex = 3;
            this.btnCreateRoute.Text = "Create hardware Route";
            this.btnCreateRoute.UseVisualStyleBackColor = true;
            this.btnCreateRoute.Click += new System.EventHandler(this.BtnCreateRoute_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(313, 435);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Edit_Route_Points);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1286, 689);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupGeneral.ResumeLayout(false);
            this.groupGeneral.PerformLayout();
            this.groupDesignerMode.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnChangeDiagramState;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnLoadDiagram;
        private System.Windows.Forms.GroupBox groupDesignerMode;
        private System.Windows.Forms.Button btnResetDiagram;
        private System.Windows.Forms.Button btnSaveDiagram;
        private System.Windows.Forms.Button btnRemoveRoute;
        private System.Windows.Forms.Button btnCreateRoute;
        private System.Windows.Forms.ListView listViewInstruments;
        private System.Windows.Forms.ComboBox ComboSecondarySelection;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupGeneral;
        private System.Windows.Forms.TextBox txtBoxState;
        private System.Windows.Forms.RichTextBox rTBgridInfo;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private ImageGridUserControl.DataGrid dataGrid1;
        private System.Windows.Forms.Button button1;
    }
}

