﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace ImageGridUserControl.Classes.Images
{

    public class ImageList
    {
        public ImageList()
        {
            _images = new List<HardwareImage>();
        }

        List<HardwareImage>  _images;
        public List<HardwareImage> Images { get { return _images; } set { _images = value; } }

        public static void SaveDefaultList(string FileName)
        {
            List<HardwareImage> Temp = new List<HardwareImage>();

            ImageList ImgList = new ImageList();
            string[] names = new string[] { "RF_Source","Network_Analyzer","Noise_Meter","DC_Power","Dut","Tuner","Controller","Bias_Tee","Switch"};

            foreach (string name in names)
            {
                Temp.Add(new HardwareImage(name, ""));
            }


            ImgList.Images = Temp;

            string file = XML_Saver.SerializeObject(ImgList);
            System.IO.File.WriteAllText(FileName, file);
        }

        internal static ImageList LoadList(string fileName)
        {
            ImageList imageList = new ImageList();
            using (var reader = new StreamReader(fileName))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(ImageList));
                imageList = (ImageList)deserializer.Deserialize(reader);
            }

            string  path = System.IO.Path.GetDirectoryName(fileName);
            imageList.LoadImages(path);

            return imageList;

        }

        private void LoadImages(string path)
        {
            foreach (HardwareImage image  in _images)
            {
                image.LoadImage(path);
            }
        }

        public List<string> GetImageTypes()
        {
            List<string> res = new List<string>();
            foreach (HardwareImage image in _images)
            {
                res.Add(image.ImageType);
            }
            return res;
        }
    }

    public class HardwareImage
    {
        private double _defaultW=800;
        private double _defaultH =800;
        private int _size;
        private double _left;
        private double _width;
        private double _height;
        private double _top;
        private string _fileName;
        private string _imageType;
        private string _itemName;
        private BitmapImage _bitmap;
        private List<ConectionPoint> _conectionPoints;

        public HardwareImage()
        {
            this._size = 1;
            this._imageType = "";
            this._itemName = "";
            this._fileName = "";
            this._conectionPoints = new List<ConectionPoint>();
            //_conectionPoints.Add(new ConectionPoint() { ConnectionName = "0", Location = new System.Windows.Point() });
        }

        public string ItemName { get { return _itemName; } set { _itemName = value; } }
        public string ImageType { get { return _imageType; } set { _imageType = value; } }
        public double left { get { return _left; } set { _left = value; } }
        public double width { get { return _width; } set { _width = value; } }
        public double height { get { return _height; } set { _height = value; } }
        public double top { get { return _top; } set { _top = value; } }
        /// <summary>
        /// 
        /// </summary>
        public int size { get { return _size; } set { _size = value; } }
        public string FileName { get { return _fileName; } set { _fileName = value; } }
        public List<ConectionPoint> ConectionPoints { get { return _conectionPoints; } set { _conectionPoints = value; } }
        [XmlIgnore]
        public BitmapImage Image { get { return _bitmap; } set { _bitmap = value; } }
        public void LoadImage(string path)
        {
            string File = path +"\\"+ _fileName;
            _bitmap = new BitmapImage(new System.Uri(File));

            //_image = new System.Windows.Controls.Image();
            //_image.Source = bitmap;

            //_image.Height = height;
            //_image.Width = width;

            //Canvas.SetLeft(_image, left);
            //Canvas.SetTop(_image, top);
        }

        public bool CheckLocation(Point mousePos,double Width,double Height)
        {
            double mouseX = mousePos.X*800/Width;
            double mouseY = mousePos.Y*800/Height;
            return IsMouseInArea(mouseX, mouseY, this.left, this.top, this.width * this.size, this.height * this.size);
        }

        private static bool IsMouseInArea(double mouseX,double mouseY, double left,double top,double width,double height)
        {
            //double xloc = mouse.X;
            //double yloc = mouse.Y;
            bool insideImageArea = (mouseX >= left) && (mouseX <= (left + width)) && (mouseY >= top) && (mouseY <= (top + height));
            return insideImageArea;
        }

        public System.Windows.Controls.Image GetImage(double Width,double Height)
        {
            System.Windows.Controls.Image i = new Image();
            i.Source = _bitmap;
            i.Height = height*size / _defaultH* Height;
            i.Width = width * size / _defaultW* Width;

            double scaleY = i.Height / _bitmap.Height;
            double scaleX = i.Width / _bitmap.Width;
            //i.LayoutTransform = new System.Windows.Media.ScaleTransform(scaleX, scaleY);
            i.Stretch = System.Windows.Media.Stretch.Fill;
            i.StretchDirection = StretchDirection.Both;
            Canvas.SetLeft(i, left/_defaultW* Width);
            Canvas.SetTop(i, top/ _defaultH* Height);
            
            return i;
        }

        public HardwareImage Copy()
        {
            HardwareImage H = new HardwareImage();
            H.Image = this.Image;
            H.height = this.height;
            H.ImageType = this.ImageType;
            H.ItemName = this.ItemName;
            H.left = this.left;
            H.size = this.size;
            H.top = this.top;
            H.width = this.width;
            H.FileName = this.FileName;
            H.ConectionPoints = new List<ConectionPoint>();
            H.ConectionPoints.AddRange(this.ConectionPoints.ToArray());
            return H;
        }
        public void SetName(List<HardwareImage> hardware)
        {
            List<HardwareImage> matches = hardware.FindAll(cl => cl.ImageType == this.ImageType);
            int idx = 0;
            int idx2 = 0;
            foreach (HardwareImage item in matches)
            {
                if (int.TryParse(item.ItemName.Replace(item.ImageType + "_", ""), out idx))
                {
                    if (idx > idx2) idx2 = idx;
                }
            }
            this.ItemName = this.ImageType + "_" + (idx2 + 1).ToString();
        }
        public void SetPosition(Point mousePos,double Width,double Height)
        {
            this.left = (int)(mousePos.X - (double)width / 2 * size/ _defaultW* Width) / Width*_defaultW;
            this.top = (int)(mousePos.Y - (double)height / 2 * size/ _defaultW* Height) / Height * _defaultH;

        }
        public Point GetLocation()
        {
            return new Point(left, top);
        }
        public HardwareImage(string imageType, string itemName)
        {
            this._size = 1;
            this._imageType = imageType;
            this._itemName = itemName;
            this._fileName = " ";
            this._conectionPoints = new List<ConectionPoint>();
            _conectionPoints.Add(new ConectionPoint() { ConnectionName = "0", Location = new System.Windows.Point() });
        }
        public string[] GetConections()
        {
            List<string> res = new List<string>();
            foreach (var item in ConectionPoints)
            {
                res.Add(this.ItemName + ":" + item.ConnectionName);
            }
            return res.ToArray();           
        }

        public ConectionPoint GetConectionPoint(string ConectionInfoString)
        {
            string Name = ConectionInfoString.Split(':')[1];
            ConectionPoint connectionPoint  = ConectionPoints.First(cl => cl.ConnectionName == Name);
            double X = connectionPoint.Location.X *this.size + this.left;
            double Y = connectionPoint.Location.Y *this.size + this.top;
            //X = X / 800 * width;
            //Y = Y / 800 * height;
            ConectionPoint connectionPointRes = new ConectionPoint() { ConnectionName = connectionPoint.ConnectionName, Location = new Point(X, Y) };
            return connectionPointRes;
        }
    }

    public class ConectionPoint
    {
        public ConectionPoint()
        {
            this._name = "";
            this._location = new System.Windows.Point();
        }
        private string _name;
        private System.Windows.Point _location;
        public string ConnectionName { get { return _name; } set { _name = value; } }
        public System.Windows.Point Location { get { return _location; } set { _location = value; } }
    }
}
