﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ImageGridUserControl.Classes.Images
{
    public class Routes
    {
        private ConectionPoint _end;
        private ConectionPoint _start;
        private List<System.Windows.Point> _waypoints;
        private string _name;

        public Routes()
        {
            _waypoints = new List<Point>();
        }

        public ConectionPoint End { get { return _end; } set { _end = value; } }
        public ConectionPoint Start { get { return _start; }  set { _start = value; } }
        public string Name { get { return _name; }  set { _name = value; } }
        public List<System.Windows.Point> Waypoints { get { return _waypoints; }  set { _waypoints = value; } }

        public Polyline GetPolyLine(double width, double height)
        {

            Polyline Current_Line = new Polyline();
            Current_Line.Stroke = System.Windows.Media.Brushes.AntiqueWhite;
            PointCollection points = new PointCollection();
            points.Add(pointConverter(this.Start.Location, width, height));

            foreach (Point LinePoint in _waypoints)
            {
                points.Add(pointConverter(LinePoint, width, height));
            }

            points.Add(pointConverter(this.End.Location, width, height));

            Current_Line.Points = points;
            Current_Line.StrokeThickness = 1;

            return Current_Line;
        }

        private Point pointConverter(Point P, double width, double height)
        {
            Point Pn = new Point(P.X * width/800d, P.Y * height / 800d);
            return Pn;
        }
    }
}
