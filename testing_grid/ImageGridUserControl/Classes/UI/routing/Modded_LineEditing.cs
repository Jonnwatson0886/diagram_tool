﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using corner.cS;
using ImageGridUserControl.Classes.Images;
using ImageGridUserControl.xaml.Diagram;
using ImageGridUserControl.xaml.Diagram.Classes;

namespace ImageGridUserControl.Classes.UI.routing
{
    class Modded_LineEditing : IRouting_Configuration 
    {
        #region vars
        private HardwareImage OriginHardware;
        private int size;
        private Multi_P_Line line;
        #endregion
        #region constructor
        public Modded_LineEditing(HardwareImage originHardware)
        {
            OriginHardware = originHardware;
            line = new Multi_P_Line();
        }
        #endregion

        public HardwareImage convertXY(direction Facing, HardwareImage holder)
        {
            HardwareImage Holder = holder;
            size = Holder.size;
            Point Location = new Point(Holder.left,Holder.top);

            return Holder;
        }
        private void CreateLine(List<Point>points)
        {
            PointCollection Points_Col = new PointCollection();
            foreach (Point LinePoint in points)
            {
                Points_Col.Add(LinePoint);
            }
            line.polyline.Points = Points_Col;
        }
        public void Grenerate_Line_Points(HardwareImage Current_Target)
        {
            List<Point> points = new List<Point>();
            Point start = OriginHardware.GetLocation();
            line.Editiable_Points[0] = new Point(start.X, start.Y);
            points.Add(line.Editiable_Points[0]);

            Point end = Current_Target.GetLocation();
            
            points.Add(new Point(end.X, end.Y));

            //draw the line
            CreateLine(points);
        }

        public Multi_P_Line LineCreator(HardwareImage Current_Target)
        {
            Grenerate_Line_Points(Current_Target);
            line.polyline.Stroke = System.Windows.Media.Brushes.AntiqueWhite;
            line.polyline.StrokeThickness = 3;
            return line;
        }
        public Multi_P_Line Return_Line(List<Point> points)
        {
            CreateLine(points);
            return line;
        }

        public void Execute(Board _board)
        {
        }

        public void Reverse(Board _board)
        {
            throw new NotImplementedException();
        }
    }
}
