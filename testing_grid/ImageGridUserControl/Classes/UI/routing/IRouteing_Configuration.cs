﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;
using ImageGridUserControl.Classes.Images;
using ImageGridUserControl.xaml.Diagram;

namespace ImageGridUserControl.Classes
{
    interface IRouting_Configuration
    {
        HardwareImage convertXY(direction Facing, HardwareImage holder);
        void Grenerate_Line_Points(HardwareImage Current_Target);
    }
}