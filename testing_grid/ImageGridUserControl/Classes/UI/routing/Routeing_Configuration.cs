﻿//using System.Collections.Generic;
//using System.Windows;
//using System.Windows.Shapes;
//using ImageGridUserControl.xaml.Diagram;

//namespace ImageGridUserControl.Classes
//{
//    /// <summary>
//    /// a class that deals with ports on a hardware tile and the side they are on
//    /// </summary>
//    class Routeing_Configuration : IRouting_Configuration
//    {
//        #region vars
//        public Point2Int Tile;
//        public int size;
//        public Line line;
//        #endregion
//        #region constructor
//        /// <summary>
//        /// constuctor that creates the points to draw to and from on a line.
//        /// </summary>
//        /// <param name="Port_Count"></param>
//        /// <param name="tile"></param>
//        public Routeing_Configuration(int Num_Of_Ports, Point2Int tile)
//        {
//            Tile = tile;
//        }
//        #endregion

//        /// <summary>
//        /// creates a line but doesn't draw it
//        /// </summary>
//        /// <param name="Current_Target"></param>
//        /// <returns></returns>
//        public void Grenerate_Line_Points(Point2Int Current_Target)
//        {
//            //create start point 
//            Point2Int start = Tile_Utils.GetPixelFromTileCoordinates(new Point2Int(Tile.X, Tile.Y));

//            start.size = Tile.size;
//            start.output_side = Tile.output_side;

//            start = convertXY(start.output_side, start);
//            //create end point 
//            Point2Int end = Tile_Utils.GetPixelFromTileCoordinates(new Point2Int(Current_Target.X, Current_Target.Y));

//            end.size = Current_Target.size;
//            end.input_side = Current_Target.input_side;

//            end = convertXY(end.input_side, end);
//            //draw the line
//            CreateLine(start.X, start.Y, end.X, end.Y);
//        }
//        /// <summary>
//        /// draws the line passed though to it 
//        /// </summary>
//        /// <param name="x1"></param>
//        /// <param name="y1"></param>
//        /// <param name="x2"></param>
//        /// <param name="y2"></param>
//        /// <param name="AddLine"></param>
//        /// <returns></returns>
//        public void CreateLine(int x1, int y1, int x2, int y2)
//        {
//            line = new Line();
//            line.StrokeThickness = 1;
//            line.Stroke = System.Windows.Media.Brushes.White;

//            line.X1 = x1;
//            line.X2 = x2;
//            line.Y1 = y1;
//            line.Y2 = y2;

//        }
//        /// <summary>
//        /// changes the connection face of the hardware icon passed though the 
//        /// </summary>
//        /// <param name="Facing"></param>
//        /// <param name="holder"></param>
//        /// <returns></returns>
//        public Point2Int convertXY(direction Facing, Point2Int holder)
//        {
//            Point2Int Holder = holder;
//            size = Holder.size;
//            int x = Holder.X;
//            int y = Holder.Y;

//            switch (Facing)
//            {
//                //mess with to cherry pick size
//                case direction.bottom:
//                    Holder.X = x + ((size * Tile_Texture_Manger.Tile_Size) / 2);
//                    Holder.Y = y + (size * Tile_Texture_Manger.Tile_Size);
//                    break;

//                case direction.right:
//                    Holder.X = x + ((size * Tile_Texture_Manger.Tile_Size));
//                    Holder.Y = y + ((size * Tile_Texture_Manger.Tile_Size) / 2);
//                    break;

//                case direction.left:
//                    Holder.Y = y + ((size * Tile_Texture_Manger.Tile_Size) / 2);
//                    break;

//                case direction.top:
//                    Holder.X = x + ((size * Tile_Texture_Manger.Tile_Size) / 2);
//                    break;
//            }
//            return Holder;
//        }
//        public Line LineCreator(Point2Int Current_Target)
//        {
//            Grenerate_Line_Points(Current_Target);
//            return line;
//        }
//        public Line Return_Existing_Line(List<Point> points)
//        {
//            if (points.Count == 2)
//            {
//                CreateLine((int)points[0].X, (int)points[0].Y, (int)points[1].X, (int)points[1].Y );
//            }
//            return line;
//        }

//        public void CreateLine(List<Point> points)
//        {

//        }
//    }
//}



