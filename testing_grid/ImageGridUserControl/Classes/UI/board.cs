﻿using corner.cS;
using ImageGridUserControl.Classes.Images;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace ImageGridUserControl.xaml.Diagram.Classes
{
    public class Board
    {
        #region var
        public double Height;
        public double Width;
        public List<HardwareImage> Hardware;
        //public List<Point2Int> tiles { get; set; } = new List<Point2Int>();
        public List<Routes> routes;
        public List<int> Highlighted_Lines;
        public string Current_Hardware;
        public string Current_Target;
        public string selected_hardware;
        public string ComboBox_Selected_Value;

        [XmlIgnore]
        public ImageList Images;
        #endregion
        #region constructor
        public Board(){}
        public Board(double H, double W)
        {
            Height = H;
            Width = W;
            Hardware = new List<HardwareImage>();
            routes = new List<Routes>();
            Current_Hardware = "";
            Current_Target = "";
            selected_hardware = "";
            ComboBox_Selected_Value = "";
            Highlighted_Lines = new List<int>();


            StringBuilder file = new StringBuilder();
            file.Append(System.IO.Directory.GetCurrentDirectory());
            file.Append("\\..\\..\\Diagram_Textures\\DefaultImageProperties.xml");

            //ImageList.SaveDefaultList("test.xml");
            //ImageList.SaveDefaultList(file.ToString());
            Images = ImageList.LoadList(file.ToString());
        }
        public bool CheckHardware(Point mousePos, out string hardwareName)
        {
            hardwareName = "";
            bool res = false;
            foreach (HardwareImage item in Hardware)
            {
                res = item.CheckLocation(mousePos, Width,Height); 
                if (res)
                {
                    hardwareName = item.ItemName;
                    break;
                }
            }


            return res;
        }
        public void AddNewInstrument(Point mousePos, string selectedInstrumentType)
        {
            if (selectedInstrumentType == null) return;
            HardwareImage H = Images.Images.First(cl => cl.ImageType == selectedInstrumentType);
            HardwareImage Hw = H.Copy();
            Hw.SetPosition(mousePos,this.Width, Height);
            Hw.SetName(Hardware);
            Hardware.Add(Hw); //need to create a name 
        }
        public void RemoveHardware(string hardwareName)
        {
            Hardware.RemoveAll(cl => cl.ItemName== hardwareName);
        }
        public void SetSize(int W, int H)
        {
            Height = H;
            Width = W;
        }
        public string[] GetAllConectionPoints()
        {
            List<string> res = new List<string>();

            foreach (var item in Hardware)
            {
                res.AddRange(item.GetConections());
            }

            return res.ToArray();
        }
        public string[] GetAllCompletedConectionPoints()
        {
            List<string> res = new List<string>();

            foreach (var item in routes)
            {
                res.Add(item.Name);
            }

            return res.ToArray();
        }
        public ConectionPoint GetConectionPoint(string start)
        {
            string Name = start.Split(':')[0];
            HardwareImage H = Hardware.First(cl => cl.ItemName == Name);
            return H.GetConectionPoint(start);
        }
        public void AddRoute(string start, string end)
        {
            ConectionPoint startPoint = this.GetConectionPoint(start);
            ConectionPoint endPoint = this.GetConectionPoint(end);
            string routeName = start + "|" + end;

            if (!routes.Exists(cl => cl.Name == routeName))
            {
                Routes Route = new Routes() { Name = routeName, Start = startPoint, End = endPoint };
                routes.Add(Route);
            }
        }
        public void Initalize(double H, double W)
        {
            StringBuilder file = new StringBuilder();
            file.Append(System.IO.Directory.GetCurrentDirectory());
            file.Append("\\..\\..\\Diagram_Textures\\DefaultImageProperties.xml");

            Images = ImageList.LoadList(file.ToString());

            string path = System.IO.Path.GetDirectoryName(file.ToString());

            foreach (var item in Hardware)
            {
                item.LoadImage(path);
            }
        }
        public void RemoveRoute(string routeName)
        {
            routes.RemoveAll(cl => cl.Name == routeName);
        }
        #endregion


    }
}
