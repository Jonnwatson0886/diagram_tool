﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace testing_grid.Classes.UI
{
    public class ChangeValHandler : INotifyPropertyChanged
    {
        #region vars

        public int Board_Width
        {
            get
            {
                return _Width;
            }
            set
            {
                if (_Width != value)
                {
                    _Width = value;
                    OnPropertyChanged();
                }
            }

        }
        private int _Width;

        public int Board_Height
        {
            get
            {
                return _Height;
            }
            set
            {
                if (_Height != value)
                {
                    _Height = value;
                    OnPropertyChanged();
                }
            }

        }
        private int _Height;


        #endregion
        #region constuctor
        public ChangeValHandler(int User_Control_Size, int User_Control_Heigh)
        {
            Resize(User_Control_Size, User_Control_Heigh);
        }
        #endregion
        /// <summary>
        /// makes the canvas 7 10th of the size of the user control
        /// </summary>
        /// <param name="user_control_Size"></param>
        public void Resize(int User_Control_Width,int User_Control_Height)
        {
            Board_Width = User_Control_Width*2;
            Board_Height = User_Control_Height;
         }
        #region event handler
        //event handler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string property = null)
        {
            PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(property));
        }
        #endregion
    }
    
}
