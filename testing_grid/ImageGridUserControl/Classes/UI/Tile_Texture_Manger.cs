﻿//using System.Collections.Generic;
//using System.Windows.Media.Imaging;

//namespace ImageGridUserControl.xaml.Diagram
//{
//    /// <summary>
//    /// stores hardware textures and deals with the size of the tiles in the diagram
//    /// </summary>
//    ///
//    class Tile_Texture_Manger
//    {
//        #region vars
//        public List<string> bitmaps;
//        BitmapImage _blank_Icon;
//        BitmapImage _bias_Tee;
//        BitmapImage _controller;
//        BitmapImage _dC_Power;
//        BitmapImage _dut;
//        BitmapImage _network_Analyzer;
//        BitmapImage _noise_Meter;
//        BitmapImage _rF_Source;
//        BitmapImage _switch;
//        BitmapImage _tuner;

//        //controls the size of the board tile sizes
//        public const int Tile_Size = 4;

//        //bitmap images ready for modifiying for the grid
//        public BitmapImage Blank_Icon { get { return _blank_Icon; } set { _blank_Icon = value; } }
//        public BitmapImage Bias_Tee { get { return _bias_Tee; } set { _bias_Tee = value; } }
//        public BitmapImage Controller { get { return _controller; } set { _controller = value; } }
//        public BitmapImage DC_Power { get { return _dC_Power; } set { _dC_Power = value; } }
//        public BitmapImage Dut { get { return _dut; } set { _dut = value; } }
//        public BitmapImage Network_Analyzer { get { return _network_Analyzer; } set { _network_Analyzer = value; } }
//        public BitmapImage Noise_Meter { get { return _noise_Meter; } set { _noise_Meter = value; } }
//        public BitmapImage RF_Source { get { return _rF_Source; } set { _rF_Source = value; } }
//        public BitmapImage Switch { get { return _switch; } set { _switch = value; } }
//        public BitmapImage Tuner { get { return _tuner; } set { _tuner = value; } }

//        #endregion
//        /// <summary>
//        /// contructor
//        /// </summary>
//        public Tile_Texture_Manger()
//        {
//            bitmaps = new List<string>();
//            bitmaps.Add("RF_Source");
//            bitmaps.Add("Network_Analyzer");
//            bitmaps.Add("Noise_Meter");
//            bitmaps.Add("DC_Power");
//            bitmaps.Add("Dut");
//            bitmaps.Add("Tuner");
//            bitmaps.Add("Controller");
//            bitmaps.Add("Bias_Tee");
//            bitmaps.Add("Switch");
//        }
//    }


//    static class Paser
//    {

//        /// <summary>
//        /// This function imports the game textures into a CGameTexture j
//        /// object from the given directory. All textures should be present.
//        /// </summary>
//        /// <param name="directory">The directory where the textures are stored</param>
//        /// <returns>An object storing the loading texteures.</returns>
//        public static Tile_Texture_Manger ImportTextures(Tile_Texture_Manger current, string directory)
//        {

//            System.Net.Cache.RequestCachePolicy P = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
//            Tile_Texture_Manger ret = current;

//            //directory = "C:\\Users\\Simon\\Desktop\\live_Diagram_software\\images_2";
//            ret.Blank_Icon = new BitmapImage(new System.Uri(directory + "\\blankImage.jpg"));
//            ret.Bias_Tee = new BitmapImage(new System.Uri(directory + "\\Bias_Tee.jpg"));
//            ret.Controller = new BitmapImage(new System.Uri(directory + "\\Controller.jpg"));
//            ret.DC_Power = new BitmapImage(new System.Uri(directory + "\\DC_Power.jpg"));
//            ret.Dut = new BitmapImage(new System.Uri(directory + "\\Dut.jpg"));
//            ret.Network_Analyzer = new BitmapImage(new System.Uri(directory + "\\Network_Analyser.jpg"));
//            ret.Noise_Meter = new BitmapImage(new System.Uri(directory + "\\Noise_Meter.jpg"));
//            ret.RF_Source = new BitmapImage(new System.Uri(directory + "\\RF_Source.jpg"));
//            ret.Switch = new BitmapImage(new System.Uri(directory + "\\Switch.jpg"));
//            ret.Tuner = new BitmapImage(new System.Uri(directory + "\\Tuner.jpg"));

//            return ret;
//        }
//    }
//}
