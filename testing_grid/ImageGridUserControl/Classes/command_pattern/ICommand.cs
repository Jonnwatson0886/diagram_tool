﻿using ImageGridUserControl.xaml.Diagram.Classes;

namespace ImageGridUserControl.Classes
{
    public interface ICommand
    {
        void Execute(Board _board);
        void Reverse(Board _board);
    }
}
