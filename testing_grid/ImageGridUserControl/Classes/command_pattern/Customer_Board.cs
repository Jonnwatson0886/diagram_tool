﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using corner.cS;
using ImageGridUserControl.Classes;
using ImageGridUserControl.Classes.UI.routing;
using ImageGridUserControl.xaml.Diagram.Classes;

namespace ImageGridUserControl.xaml.Diagram
{
    class Customer_Board : ICommand
    {
        //#region vars
        //Board board;
        //#endregion
        ///// <summary>
        ///// highlight the route clicked on
        ///// </summary>
        ///// <param name="_board"></param>
        public void Execute(Board _board) { }
        //{
        //    //get the main board
        //    board = _board;
        //    //create a blank line to use as a host 
        //    Multi_P_Line highlightRoute = new Multi_P_Line();

        //    //Point2Int container = new Point2Int();

        //    //set the starting point of the line to the tile selected tile object 
        //   // highlightRoute.polyline.Points[0] = new Point(board.Current_Hardware.X, board.Current_Hardware.Y);

        //    //go though and set the end points to all the routes that start at the selected tile object
        //    for (int itr = 0; itr < board.Hardware.Count; itr++)
        //    {
        //        //get the line end

        //        //Routeing_Configuration _Configuration = new Routeing_Configuration(board.Current_Tile.ports_Num, board.Current_Tile);
        //        //highlightRoute = _Configuration.LineCreator(board.Hardware[itr]);

        //        Modded_LineEditing get_route = new Modded_LineEditing(board.Current_Hardware);
        //        highlightRoute = get_route.LineCreator(board.Hardware[itr]);
        //        //create a string to compair against the routes list
        //        string highlightRoute_String = convert_line_to_string(highlightRoute);
        //        //loop though the routes and if they match fire off function to make route thicker
        //        for (int i = 0; i < board.Routes.Count; i++)
        //        {
        //            //make the route from list into a string 
        //            string List_Route = convert_line_to_string(board.Routes[i]);
        //            //compair them and see if they match
        //            if (List_Route == highlightRoute_String)
        //            {
        //                board.Routes[i].polyline.StrokeThickness = 4;
        //                board.Highlighted_Lines.Add(i);
        //            }
        //        }
        //    }
        //    _board = board;
        //}
        ///// <summary>
        ///// reverse the highlighting method
        ///// </summary>
        ///// <param name="_board"></param>
        public void Reverse(Board _board) { }
        //{
        //    board = _board;
        //    foreach (Multi_P_Line line in board.Routes)
        //    {
        //        line.polyline.StrokeThickness = 1;
        //    }
        //    board.Highlighted_Lines.Clear();
        //    _board = board;
        //}
        ///// <summary>
        ///// takes a route line and converts the points to a string
        ///// </summary>
        ///// <param name="line">The string to be converted</param>
        ///// <returns>a string of all the x,y values</returns>
        //private string convert_line_to_string(Multi_P_Line line)
        //{
        //    StringBuilder line_String = new StringBuilder();
        //    for(int itr =0;  itr < line.polyline.Points.Count; itr++)
        //    {
        //      line_String.Append(line.polyline.Points[itr].ToString());
        //        if (itr != ((line.polyline.Points.Count - 1)))
        //         { 
        //            line_String.Append(",");
        //        }
        //    }
        //    //return converted string 
        //    return line_String.ToString();
        //}
    }
}
