﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageGridUserControl.Classes.Events
{
    public class GridInfo
    {
        public GridInfo()
        {
            _unSelectedInstruments = new string[0];
            _designerModeInfo = new DesignerInfo();
        }
        public bool InstrumentWasSelected
        {
            get { return _instrumentWasSelected; }
            set { _instrumentWasSelected = value; }
        }
        public string SelectedInstrumentName
        {
            get { return _selectedInstrumentName; }
            set { _selectedInstrumentName = value; }
        }
        public string[] UnSelectedInstruments
        {
            get { return _unSelectedInstruments; }
            set { _unSelectedInstruments = value; }
        }
        public DesignerInfo designerModeInfo
        {
            get { return _designerModeInfo; }
            set { _designerModeInfo = value; }
        }
        private DesignerInfo _designerModeInfo;
        private string _selectedInstrumentName;
        private string[] _unSelectedInstruments;
        private bool _instrumentWasSelected;
    }

    public class DesignerInfo
    {
        private string[] _connections;
        private string[] _completedConnections;
        public DesignerInfo()
        {
            _connections = new string[0];
            _completedConnections = new string[0];
        }
        public string[] Connections
        {
            get { return _connections; }
            set { _connections=value; }
        }

        public string[] CompletedConnections
        {
            get { return _completedConnections; }
            set { _completedConnections = value; }
        }
    }

        public class ReqestedInfo
    {
        string _secondarySelection;
        string _selectedType;

        public string SecondarySelection
        {
            get { return _secondarySelection; }
            set { _secondarySelection = value; }
        }
        public string SelectedType
        {
            get { return _selectedType; }
            set { _selectedType = value; }
        }

    }
}
