﻿using System.Collections.Generic;
using System.Xml.Serialization;
using ImageGridUserControl.Classes.Images;
using ImageGridUserControl.xaml.Diagram;

namespace ImageGridUserControl.Classes
{
    /// <summary>
    /// simple class to store key values of the diagarm
    /// </summary>
    [XmlRoot("saveClass")]
    //[XmlInclude(typeof(Point2Int))]
    //[XmlInclude(typeof(eTileType))]
    //[XmlInclude(typeof(Line))]
    public class saveClass
    {
        public List<HardwareImage> HardwareList;
        //public List<Point2Int> _Tiles;
        public List<string> Route_Info;
    }
}

