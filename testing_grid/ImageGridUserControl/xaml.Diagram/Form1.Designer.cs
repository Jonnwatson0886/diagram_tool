﻿namespace testing_grid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.show = new System.Windows.Forms.Button();
            this.host = new System.Windows.Forms.Integration.ElementHost();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.dataGrid2 = new testing_grid.DataGrid();
            this.SuspendLayout();
            // 
            // show
            // 
            this.show.Image = ((System.Drawing.Image)(resources.GetObject("show.Image")));
            this.show.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.show.Location = new System.Drawing.Point(516, 12);
            this.show.Name = "show";
            this.show.Size = new System.Drawing.Size(128, 52);
            this.show.TabIndex = 1;
            this.show.UseVisualStyleBackColor = true;
            this.show.Click += new System.EventHandler(this.show_Click);
            // 
            // host
            // 
            this.host.Location = new System.Drawing.Point(-1, 70);
            this.host.Name = "host";
            this.host.Size = new System.Drawing.Size(1984, 694);
            this.host.TabIndex = 2;
            this.host.Text = "elementHost1";
            this.host.Child = null;
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(-1, 70);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1984, 694);
            this.elementHost1.TabIndex = 3;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.dataGrid2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1984, 761);
            this.Controls.Add(this.elementHost1);
            this.Controls.Add(this.host);
            this.Controls.Add(this.show);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button show;
        private System.Windows.Forms.Integration.ElementHost host;
        private DataGrid dataGrid1;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private DataGrid dataGrid2;
    }
}

