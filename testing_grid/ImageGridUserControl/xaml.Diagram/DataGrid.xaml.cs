﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Xml.Serialization;
using corner;
using corner.cS;
using ImageGridUserControl.Classes;
using ImageGridUserControl.Classes.command_pattern;
using ImageGridUserControl.Classes.Events;
using ImageGridUserControl.Classes.Images;
using ImageGridUserControl.Classes.UI;
using ImageGridUserControl.Classes.UI.routing;
using ImageGridUserControl.xaml.Diagram;
using ImageGridUserControl.xaml.Diagram.Classes;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;

namespace ImageGridUserControl
{
    /// <summary>
    /// used to give the side a route should connect to.
    /// </summary>
    public enum direction
    {
        right,
        left,
        top,
        bottom
    }
    public enum GridState
    {
        Developer,
        Customer
    }

    public delegate void ReportMouse(double X, double Y);
    public delegate void GridChanged(GridInfo CurrentInfo);
    public delegate ReqestedInfo RequestDesignerInfo();
    /// <summary>
    /// Interaction logic for DataGrid.xaml
    /// </summary>
    /// 
    public partial class DataGrid : INotifyPropertyChanged
    {
        public event GridChanged GridChangedEvent;
        public event ReportMouse ReportMouseEvent;
        public string ImagesPath
        {
            get { return _imagesPath; }
            set { _imagesPath = value; }
        }
        public GridState DiagramUseMode
        {
            get { return _diagramUseMode; }
            set { _diagramUseMode = value; }
        }
        public int WidthExt
        {
            get { return _width; }
            set
            {
                if (value != _width)
                {
                    _width = value;
                    onPropertyChanged();
                }
            }
        }
        public int HeightExt
        {
            get { return _height; }
            set {
                if (value != _height)
                {
                    _height = value;
                    onPropertyChanged();
                }
            }
        }

        #region vars
        public string SelectedInstrumentType;
        string _imagesPath;
        //StringBuilder dir;
        Board Current_Diagram;
        GridState _diagramUseMode;
        int _width;
        int _height;
        RealTime_Render realTime_Render;

        #endregion
        #region constructors
        /// <summary>
        /// construtor requires nothing to be passed though to the user control
        /// </summary>
        public DataGrid()
        {
            InitializeComponent();
            DataContext = this;
            _diagramUseMode = GridState.Developer;
        }
        /// <summary>
        /// call that makes UC build 
        /// </summary>
        public void StartupDataGrid()
        {
            #region blank vars
            //route
            //dir = new StringBuilder();
            //dir.Append(System.IO.Directory.GetCurrentDirectory());
            //dir.Append("\\..\\..\\Diagram_Textures");
            //int H = (int)dave.ActualHeight;
            //int W = (int)dave.ActualWidth;


           // path = dir.ToString();

            //Current_Diagram = new Board((int)this.MainDIsplay.Height, (int)this.MainDIsplay.Height);
            Current_Diagram = new Board(HeightExt, WidthExt);
            realTime_Render = new RealTime_Render(ref MainDIsplay);
            #endregion
            //get the images loaded into memory
            //run the builder for the diagram

            //List<eTileType> tileTypes = new List<eTileType>();
            //try
            //{
            //    if (textures == null) return;
            //    if (textures.bitmaps == null) return;
            //    for (int i = 0; i < textures.bitmaps.Count; i++)
            //    { 
            //        //adds text of hardware to the ComboBox
            //        string item = textures.bitmaps[i];
            //        //HardwareTree.Items.Add(item);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.MessageBox.Show(ex.Message.ToString());
            //}

            //InitializeComponent();
        }
        #endregion
        public void ResetDiagram()
        {
            //comboBox.ItemsSource = null;
            if (Current_Diagram != null)
            {
                Current_Diagram = new Board(Current_Diagram.Height, Current_Diagram.Width);
                Current_Diagram.routes = new List<Routes>();
                //drawDiagram();
            }
        }
        public void SaveDiagram(string Filename)
        {
            //clear the route infomation
            string diagram = XML_Saver.SerializeObject<Board>(Current_Diagram);
            using (StreamWriter sw = new StreamWriter(Filename))
            {
                sw.WriteLine(diagram);
            }
        }
        public void InstrumentSelectionUpdated(string instrumentType)
        {
            SelectedInstrumentType = instrumentType;
        }
        public void SetSize(int width, int height)
        {
            DataContext = this;
            WidthExt = width;
            HeightExt = height;

            if (Current_Diagram != null)
            {
                Current_Diagram.SetSize(width, height);
                realTime_Render.Render(Current_Diagram);
                //NewDiagram();
            }

            //ResetDiagram();
        }
        public void LoadDiagram(string FileName)
        {
            try
            {
                saveClass load = new saveClass();

                using (var reader = new StreamReader(FileName))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Board));
                    Current_Diagram = (Board)deserializer.Deserialize(reader);
                }

                Current_Diagram.Initalize(this.HeightExt,this.WidthExt);
                realTime_Render.Render(Current_Diagram);
                //comboBox.ItemsSource = null;
                // Current_Diagram = new Board((int)MainDIsplay.Height, (int)MainDIsplay.Width);
                // Current_Diagram.Routes = new List<Routes>();
                // //Current_Diagram.Hardware = new List<Point2Int>();

                // //NewDiagram();
                // Current_Diagram.Hardware = load.HardwareList;

                // Restore_Hardware();
                // //Current_Diagram.Hardware = Check_Current_Hardware();
                // //restore_Routes(load.Route_Info);
                // //takes the restored routes and adds the route infomation into the save route
                // foreach (Multi_P_Line line in Current_Diagram.Routes)
                //     for (int itr = 0; itr < line.polyline.Points.Count; itr++)
                //     {
                //         Current_Diagram.Route_Info.Add(line.polyline.Points[itr].ToString() + ",");
                //     };
                ////drawDiagram();

            }
            catch (Exception E)
            {
                throw E;
                //System.Windows.MessageBox.Show("Error cannot read in file" + "\n" + " " + E.Message);
            }

            // Current_Diagram.tiles = load._Tiles;
        }
        public List<string> GetInstrumentNames()
        {
            List<string> Res = Current_Diagram.Images.GetImageTypes();
            return Res;
        }
        public void SecondarySelectionUpdated(string InstrumentName)
        {
            Current_Diagram.ComboBox_Selected_Value = InstrumentName;
        }
        public void CreateRoute(string start, string end)
        {
            Current_Diagram.AddRoute(start,end);
            realTime_Render.Render(Current_Diagram);
            GridChanged();
        }
        public bool TestRoute(string start, string end)
        {
            //may need other catches in here
            if (start == "") return false; //missing connection
            if (end == "") return false;  //missing connection
            if (start == end) return false; //same connection
            if (start.Split(':')[0] == end.Split(':')[0]) return false; //same instrument
            return true;

        }
        public void RemoveRoute(string RouteName)
        {
            Current_Diagram.RemoveRoute(RouteName);
            realTime_Render.Render(Current_Diagram);
            GridChanged();
        }
        public void change_Grid_State(GridState State)
        {
            _diagramUseMode = State;
        }
        /// <summary>
        /// returns the xy of the mouse on the main canvas at time of left mouse button click whilst on the canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddRemoveInstrument(object sender, MouseButtonEventArgs e)
        {
            //Current_Diagram.Current_Hardware = new Point2Int();
            //get the mouse positon in real in at point of clicking
            Point mousePos = e.GetPosition(MainDIsplay);
            //Point2Int mouseTilePos = Get_Tile_From_Mouse(mousePos.X, mousePos.Y);
            //Current_Diagram.Current_Tile = mouseTilePos;
            //check if the developer mode is enabled or not
            if (_diagramUseMode== GridState.Developer)
            {

                string HardwareName = "";
                if (Current_Diagram.CheckHardware(mousePos,out HardwareName))
                {
                    Current_Diagram.RemoveHardware(HardwareName); //remove hardware and redraw
                }
                else
                {
                    Current_Diagram.AddNewInstrument(mousePos, SelectedInstrumentType); //new hardware can be added
                }

                realTime_Render.Render(Current_Diagram);

                GridChanged();

                //start the controller object
                //Remote remote = new Remote(Current_Diagram);
                //create a developer board
                //Developer_Board developer_Board = new Developer_Board();
                //send command baised on the contents of tile clicked on
                //if (mouseTilePos.tileType != Current_Diagram.selected_hardware)
                //{
                //    remote.Invoke(developer_Board);
                //}
                //else
                //{
                //    remote.Undo(developer_Board);
                //}
                // drawDiagram();
            }
        }
        public void GridChanged()
        {
            GridInfo Info = new GridInfo();

            List<string> Unselected = new List<string>();
            foreach (var item in Current_Diagram.Hardware)
            {
                Unselected.Add(item.ItemName);
            }
            Info.InstrumentWasSelected = false;
            Info.UnSelectedInstruments = Unselected.ToArray();
            Info.SelectedInstrumentName = "";

            Info.designerModeInfo.Connections = Current_Diagram.GetAllConectionPoints();
            Info.designerModeInfo.CompletedConnections = Current_Diagram.GetAllCompletedConectionPoints();
            GridChangedEvent(Info);
        }
        /// <summary>
        /// this function takes a image in and changes the source to the 
        /// directory you pass though to it.
        /// redraws all the images in the grid when called
        /// </summary>
        /// <param name="dir">the directory of the image we want to change it to</param>
        /// <param name="image"></param>
        /// <returns>the image with a new referance</returns>
        private void MainDIsplay_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            AddRemoveInstrument(sender, e);
        }
        /// <summary>
        /// when used gives us the area we clicked on and the infomation of the area. if hardware we get the details
        /// and fill the combobox on the display with the names of the hardware we can connect to.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainDIsplay_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            GridInfo Info = new GridInfo();

            Remote remote = new Remote(Current_Diagram);
            Customer_Board customer_Board = new Customer_Board();
            remote.Undo(customer_Board);

            //comboBox.ItemsSource = null;
            //for the combobox later
            List<string> nameOfHardware = new List<string>();
            //get the area on the screen we clicked on in pixels
            Point mouse = e.GetPosition(MainDIsplay);
            //set the currernt tile to the tile the mouse is in.
            //Current_Diagram.Current_Tile = Get_Tile_From_Mouse(mouse.X, mouse.Y);
            //if in dev mode create the dropbox
            if (_diagramUseMode == GridState.Developer)
            {
                string HardwareName = "";
                if (Current_Diagram.CheckHardware(mouse, out HardwareName))
                {
                    List<HardwareImage> images = Current_Diagram.Hardware.FindAll(cl => cl.ItemName != HardwareName);
                    List<string> Unselected = new List<string>();
                    foreach (var item in images)
                    {
                        Unselected.Add(item.ItemName);
                    }
                    Info.SelectedInstrumentName = HardwareName;
                    Info.UnSelectedInstruments = Unselected.ToArray();
                    Info.InstrumentWasSelected = true;
                    GridChangedEvent(Info);
                }
                else
                {
                    List<string> Unselected = new List<string>();
                    foreach (var item in Current_Diagram.Hardware)
                    {
                        Unselected.Add(item.ItemName);
                    }


                    Info.InstrumentWasSelected = false;
                    Info.UnSelectedInstruments = Unselected.ToArray();
                    Info.SelectedInstrumentName = "";
                    GridChangedEvent(Info);
                }


                //see if the tile we are on is hardware and in the grid
                //if (Current_Diagram.Current_Hardware.tileType != eTileType.blank)
                //{
                //    //remove the selected tile from the list
                //    Info.SelectedInstrumentName = Current_Diagram.Current_Hardware.Name;

                //    Current_Diagram.Hardware.Remove(Current_Diagram.Current_Hardware);


                //    //make a suiatble list of labels of the hardware for the user
                //    for (int i = 0; i < Current_Diagram.Hardware.Count; i++)
                //    {
                //        //nameOfHardware.Add(Current_Diagram.Hardware[i].Name);
                //    }
                //    Info.UnSelectedInstruments = nameOfHardware.ToArray();

                //    //do the combo box
                //    //comboBox.ItemsSource = nameOfHardware;
                //    //readd the seleted tile back to hardware referance list
                //    //Current_Diagram.Hardware = Check_Current_Hardware();

                    

                //}
                //else
                //{
                //    //comboBox.ItemsSource = null;
                //}


            }
            else
            {
                //if (Current_Diagram.Current_Hardware.tileType != eTileType.blank)
                //{
                //    remote.Invoke(customer_Board);
                //}
                //else
                //{
                //    remote.Undo(customer_Board);
                //}
            }

            //make the label the hardware clicked on
            //currenty_seleted_Label.Content = Current_Diagram.Current_Tile.Name;
        }
        /// <summary>
        /// resets the board to a blank board
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_but_Click(object sender, RoutedEventArgs e)
        {
            Current_Diagram = new Board(Current_Diagram.Height, Current_Diagram.Width);
        }
        #region property changed event 
        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged([CallerMemberName] string property_name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property_name));
        }

        #endregion

        private void MainDIsplay_MouseMove(object sender, MouseEventArgs e)
        {
            if (_diagramUseMode == GridState.Developer)
            {
                Point mouse = e.GetPosition(MainDIsplay);

                ReportMouseEvent(mouse.X, mouse.Y);
            }
        }
    }
}


