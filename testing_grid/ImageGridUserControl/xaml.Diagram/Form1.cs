﻿using System;
using System.Windows.Forms;

namespace testing_grid
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            host.Visible = true;
            //show.Text = "turn to developer mode " + dataGrid1.dev.ToString();
        }

        private void show_Click(object sender, EventArgs e)
        {
            dataGrid1.change_developer_State();
            host.Visible = false;
            //show.Text = "turn to developer mode " + dataGrid1.dev.ToString();

        }
    }
}
