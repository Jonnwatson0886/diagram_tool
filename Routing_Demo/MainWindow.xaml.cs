﻿using corner.cS;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;


namespace corner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Vars
        RealTime_Render realTime_;
        List<Multi_P_Line> polylines;
        #endregion
        #region constructor
        public MainWindow()
        {
            InitializeComponent();
            realTime_ = new RealTime_Render(ref testBed);
            polylines = new List<Multi_P_Line>();
            DataContext = realTime_;
        }
        #endregion
        private void click(object sender, MouseButtonEventArgs e)
        {
            if (polylines.Count != 0)
            {
                if (!realTime_.stopper)
                {
                    realTime_.stopper = true;
                    realTime_.start_timer(sender);
                }
                else
                {
                    realTime_.stopper = false;
                }
            }
        }
        private void Thickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           realTime_.Draw_PolyLine();
        }          

        private void Route_List_DropDownOpened(object sender, EventArgs e)
        {
            List<int> _ints = new List<int>();

            for (int itr = 0; itr < polylines.Count; itr++)
            {
                _ints.Add(itr);
            }
            Route_List.ItemsSource = _ints;
        }

        private void Route_List_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            realTime_.set_selected(polylines[(int)Route_List.SelectedItem]);
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            polylines.Add(new Multi_P_Line());
        }

        private void Add_Line_Point_Click(object sender, RoutedEventArgs e)
        {
            polylines[(int)Route_List.SelectedItem].Editiable_Points.Insert(1,new Point(0, 0));
        }

        private void Elispe_Box_DropDownOpened(object sender, EventArgs e)
        {
            Elispe_Box.Items.Clear();
            if (Route_List.SelectedItem != null)
            {
                for (int itr = 0; itr < polylines[(int)Route_List.SelectedItem].Editiable_Points.Count; itr++)
                {
                    Elispe_Box.Items.Add(itr + 1);
                }
            }
        }

        private void Elispe_Box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Elispe_Box.SelectedItem != null)
            {
                realTime_.set_Index((int)Elispe_Box.SelectedItem -1 );
            }
        }
    }
}   