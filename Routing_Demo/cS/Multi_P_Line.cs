﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;

namespace corner.cS
{
    class Multi_P_Line
    {
        #region vars
     public Polyline polyline;
     public List<Point> Editiable_Points;
     private Point start;
     private Point end;
        #endregion
        #region constructor
        public Multi_P_Line(int Number_Of_Points , Point Start, Point End)
        {
            polyline = new Polyline();
            start = Start;
            end = End;
            createPoints(Number_Of_Points);
        }
        public Multi_P_Line()
        {
            polyline = new Polyline();
            start = new Point(5,5);
            end = new Point(250,250);
            createPoints(1);
        }

        #endregion
        /// <summary>
        /// creates the list of points
        /// </summary>
        /// <param name="count"></param>
        private void createPoints(int count)
        {
            Editiable_Points = new List<Point>();
            Editiable_Points.Add(start);
            for (int itr = 0; itr < count; itr++)
            {
                Editiable_Points.Add(new Point(0,0));
            }
            Editiable_Points.Add(end);
        }

    }



}
